<?php

/*Asset Default status*/
define('ASSET_UNDEPLOY', 1);
define('ASSET_DEPLOY', 2);
define('ASSET_PENDING', 3);
define('ASSET_DAMAGE', 4);
define('ASSET_INSERVICE', 5);
define('ASSET_DISPOSED', 6);
define('ASSET_REJECTED', 7);

define('CURRENCY','Rs.');

define('BARCODE_TYPE','C128B');

define('BARCODE_TYPE_ID','3');

/*GRN Asset*/
define('CHECKED',1);
define('REJECTED',2);
?>