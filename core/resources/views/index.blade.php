@extends('layouts.sammy_new.master') @section('title','You are in Index Page.')
@section('content')
<div class="eq-col" style="background-color: white">
    <div class="relative full-height">
        <div class="display-row">
        <!-- error wrapper -->
        <div class="center-wrapper error-page">
            <br><br><br><br><br><br>
            <div class="col-md-8">
                <br>
                <div class="center-content text-center" style="padding-left: 250px">
                    <div class="error-number" style="font-size: 75px">
                        <span>Asset Tracking</span>
                    </div>
                    <div class="h5">Welcome to Asset Tracking. Nothing to worry about your assets.</div>
                    <p>We keep your asset in tracks</p>
                    <a class="btn btn-primary" href="{{url('dashboard')}}" style="padding: 15px 15px;margin-right: 5%">
                        <span style="border: 1px solid;padding: 5px;"><i class="fa fa-tachometer"></i> Dashboard</span>
                    </a>
                    <a class="btn btn-success" href="{{url('asset/list')}}" style="padding: 15px 15px;margin-right: 5%">
                        <span style="border: 1px solid;padding: 5px;"><i class="fa fa-barcode"></i> Asset List</span>
                    </a>
                    <a class="btn btn-default" href="{{url('check-inout/checkin')}}" style="padding: 15px 15px">
                        <span style="border: 1px solid;padding: 5px;"><i class="fa fa-check"></i>Check-In/Out</span>
                    </a>
                </div>                    
            </div>
            <div class="col-md-4">
                <div class="center-content text-center">
                    
                </div>
            </div>
            
            <!-- /error wrapper -->
        </div>
    </div>
</div>
@stop
