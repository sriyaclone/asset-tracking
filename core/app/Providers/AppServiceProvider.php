<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application views.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application views.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
