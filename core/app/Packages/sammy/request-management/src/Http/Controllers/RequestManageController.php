<?php
namespace Sammy\RequestManage\Http\Controllers;

use App\Exceptions\TransactionException;
use App\Http\Controllers\Controller;
use App\Models\RequestType;
use App\Models\RequestStatus;

use Sammy\RequestManage\Http\Requests\RequestManage;
use Sammy\RequestManage\Models\RequestDetail;
use Sammy\RequestManage\Models\RequestApprove;

use Sammy\EmployeeManage\Models\Employee;
use Sammy\AssetModal\Models\AssetModal;
use Sammy\AssetManage\Models\Asset;
use Sammy\Permissions\Models\Permission;
use Sammy\CheckInout\Models\IssueNote;
use Sammy\CheckInout\Models\IssueNoteDetails;

use App\Classes\UserLocationFilter;

use DB;
use Log;
use Response;
use Request;
use Sentinel;
use Carbon\Carbon;

class RequestManageController extends Controller {

	/*
		|--------------------------------------------------------------------------
		| Request Manage Controller
		|--------------------------------------------------------------------------
		|
		| This controller renders the "marketing page" for the application and
		| is configured to only allow guests. Like most of the other sample
		| controllers, you are free to modify or remove it as you desire.
		|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('guest');
	}

	/**
	 * Show the location add screen to the user.
	 *
	 * @return Response
	 */
	public function addView() {

		$type=RequestType::all()->lists('name','id');
		
		$modal=AssetModal::where('status',1)->lists('name','id')->prepend('Please Select..',0);
		
		return view('requestManage::request.add')->with(['type' => $type,'modal' => $modal]);
	}

	/**
	 * Get asset of the location add screen to the user.
	 *
	 * @return Response
	 */
	public function getAsset(RequestManage $request) {
		
		$locations = UserLocationFilter::getLocationOfUser();

		$data = Asset::where('asset_model_id',$request->modal)->with(['depreciationtype','transaction.status','transaction.location','assetmodel','manufacturerName']);

        $data  = $data->whereIn('id',function($aa) use($locations)
            {
                $aa->select('asset_id')->from('asset_transaction')->whereIn('location_id',$locations)->whereNull('deleted_at');
        });

        $issues = IssueNote::with(['details','grn'])->get();

        $notcheckIn=[];

        foreach ($issues as $value) {
            if($value->grn==null){

                foreach ($value->details as $ass) {
                    // if($ass->status==0){
                     $notcheckIn[]=$ass->asset_id;
                    // }
                }
            }else{
                foreach ($value->details as $ass) {
                    if($ass->status==0){
                     $notcheckIn[]=$ass->asset_id;
                    }
                }
            }
        }

        return $data=$data->whereNotIn('id',$notcheckIn)->get();
        
        // return $data->select(DB::raw('CONCAT("SN : ",asset_no," - INO : ",inventory_no) as name'))->get();
	}

	/**
	 * Add new request data to database
	 *
	 * @return Redirect to request add
	 */
	public function add(RequestManage $request) {

		// return $request->all();

		try {
			DB::transaction(function () use ($request) {

				$res;

				if($request->get('type')==1){

					$res=RequestDetail::create([
						'send_by'=>Sentinel::getUser()->employee_id,
						'request_type_id'=>$request->get('type'),
						'remark'=>$request->get('remark'),
						'asset_type_id'=>$request->get('modal'),
						'quantity'=>$request->get('quantity')
					]);					

				}else{

					$check_asset=Asset::find($request->get('asset_no'));

					if($check_asset){
						$res=RequestDetail::create([
							'send_by'=>Sentinel::getUser()->employee_id,
							'request_type_id'=>$request->get('type'),
							'asset_type_id'=>$check_asset->asset_model_id,
							'asset_no'=>$check_asset->asset_no,
							'remark'=>$request->get('remark')
						]);
					}else{
						throw new TransactionException('Something wrong.Employee wasn\'t created', 100);
					}

				}

				if ($res) {

					$employee= Employee::find(Sentinel::getUser()->employee_id);

					if($employee->employee_type_id==1){
						$approved_employee_id=$employee->id;						
					}else{
						$parent=$employee->parent()->get();

						$approved_employee_id=$parent[0]->id;						
					}


					$result=RequestApprove::create([
						'request_id'=>$res->id,
						'approved_employee_id'=>$approved_employee_id,
						'status_id'=>1
					]);
				}else{
					throw new TransactionException('Something wrong.Employee wasn\'t created', 101);
				}
			});
			return redirect('request/add')->with(['success' => true,
				'success.message' => 'Request Send successfully!',
				'success.title' => 'Good Job!']);
		} catch (TransactionException $e) {
			if ($e->getCode() == 100) {
				Log::info("Request Not added");
				return redirect('request/add')->with(['error' => true,
					'error.message' => "Request Not added. Asset not found. Try again",
					'error.title' => 'Ops!']);
			} elseif ($e->getCode() == 101) {
				Log::info('Request Not added');
				return redirect('request/add')->with(['error' => true,
					'error.message' => "Request Not added. Try again",
					'error.title' => 'Ops!']);
			}
		} catch (Exception $e) {
			Log::info('Transaction Error');
				return redirect('request/add')->with(['error' => true,
					'error.message' => 'Transaction Error',
					'error.title' => 'Ops!']);
		}

	}

	/**
	 * Show the Location List view to the user.
	 *
	 * @return Response
	 */
	public function listView(RequestManage $request) {

		$users=UserLocationFilter::getLocationsId();

		$users=array_flatten($users);

        array_push($users, Sentinel::getUser()->employee_id);

		$data = RequestDetail::whereIn('send_by',$users)->with(['requestType','employeeApprove','requestStatus','employee.Location.location','asset.assetmodel','requestModal']);

		$type=$request->type;
        if($type!=null && $type!="" && $type!=0){
            $data = $data->where('request_type_id',$type);
        }

        $status=$request->status;
        if($status!=null && $status!="" && $status!=0){
            $data = $data->where('status',$status);
        }

		$data=$data->paginate();
		
		$count=$data->total();

		$logged_emp=Sentinel::getUser()->employee_id;

		$typeList=RequestType::all()->lists('name','id')->prepend('All',0);

		$statusList=RequestStatus::all()->lists('name','id')->prepend('All',0);

		return view('requestManage::request.list')->with([
			'data'=>$data,
			'row_count'=>$count,
			'logged'=>$logged_emp,
			'typeList'=>$typeList,
			'statusList'=>$statusList,
			'old'=>['type'=>$type,'status'=>$status]
		]);
	}

	/**
	 * Show the Location List screen to the user.
	 *
	 * @return Response
	 */
	public function jsonList(RequestManage $request) {
		if ($request->ajax()) {

			$i = 1;
			$jsonList = array();

			$requestData = RequestDetail::with(['requestType','employeeApprove','requestStatus','employee.Location.location','asset.assetmodel','requestModal'])->get();

			if (!empty($requestData)) {

				foreach ($requestData as $value) {
					$dd = array();
					array_push($dd, $i);
					
					array_push($dd, $value->employee->first_name." ".$value->employee->last_name);
					array_push($dd, $value->employee->Location!=null?$value->employee->Location->location->name:'-');
					array_push($dd, $value->requestType->name);
					array_push($dd, $value->remark);
					
					if($value->requestStatus->name=="Pending"){
						array_push($dd, "<label class='badge label-warning'>".$value->requestStatus->name."</label>");
					}else if($value->requestStatus->name=="Approved"){
						array_push($dd, "<label class='badge label-success'>".$value->requestStatus->name."</label>");
					}else if($value->requestStatus->name=="Rejected"){
						array_push($dd, "<label class='badge label-danger'>".$value->requestStatus->name."</label>");
					}else if($value->requestStatus->name=="Service"){
						array_push($dd, "<label class='badge label-info'>".$value->requestStatus->name."</label>");
					}

					if ($value->asset != NULL) {
						array_push($dd, $value->asset->assetmodel->name);
						array_push($dd, '-');
						array_push($dd, $value->asset->asset_no);
					} else {
						array_push($dd, $value->requestModal->name);
						array_push($dd, $value->quantity);
						array_push($dd, "-");
					}

					if(count($value->employeeApprove)>0){
						$emp='';
						foreach ($value->employeeApprove as $sub) {
							if($sub->approved_date==null){
								$emp=$sub->approved_employee_id;
							}
						}

						if($emp!='' && $emp==Sentinel::getUser()->employee_id){
							if($value->status==1){
								array_push($dd, '<button class="btn btn-default btn-xs request-reject" data-toggle="tooltip" data-placement="top" data-id="'.$value->id.'" onclick="rejectFunc('.$value->id.')" title="Reject"><i class="fa fa-trash-o"></i> Reject</button>');
								array_push($dd, '<button class="btn btn-default btn-xs request-approve" data-toggle="tooltip" data-placement="top" data-id="'.$value->id.'" onclick="approveFunc('.$value->id.')" title="Approve"><i class="fa fa-check"></i> Approve</button>');
							}else if($value->status==2){
								array_push($dd, '<button class="btn btn-default btn-xs disabled" data-toggle="tooltip" data-placement="top" title="Reject Disabled"><i class="fa fa-trash-o"></i> Reject</button>');
								array_push($dd, '<span class="label label-success" style="padding: .6em 10px .3em;">Approved</span>');
							}else if($value->status==3){
								array_push($dd, '<span class="label label-danger" style="padding: .6em 10px .3em;">Rejected</span>');
								array_push($dd, '<button class="btn btn-default btn-xs disabled" data-toggle="tooltip" data-placement="top" title="Approve Disabled"><i class="fa fa-check"></i> Approve</button>');
							}else if($value->status==4){
								array_push($dd, '<button class="btn btn-default btn-xs disabled" data-toggle="tooltip" data-placement="top" title="Reject Disabled"><i class="fa fa-trash-o"></i> Reject</button>');
								array_push($dd, '<button class="btn btn-default btn-xs disabled" data-toggle="tooltip" data-placement="top" title="Approve Disabled"><i class="fa fa-check"></i> Approve</button>');
							}
						}else{
							array_push($dd, '<button class="btn btn-default btn-xs disabled" data-toggle="tooltip" data-placement="top" title="Reject Disabled"><i class="fa fa-trash-o"></i> Reject</button>');
							array_push($dd, '<button class="btn btn-default btn-xs disabled" data-toggle="tooltip" data-placement="top" title="Approve Disabled"><i class="fa fa-check"></i> Approve</button>');
						}						
					}else{
						if($value->status==1){
							array_push($dd, '<button class="btn btn-default btn-xs disabled" data-toggle="tooltip" data-placement="top" title="Reject Disabled"><i class="fa fa-trash-o"></i> Reject</button>');
							array_push($dd, '<button class="btn btn-default btn-xs disabled" data-toggle="tooltip" data-placement="top" title="Approve Disabled"><i class="fa fa-check"></i> Approve</button>');
						}else if($value->status==2){
							array_push($dd, '<button class="btn btn-default btn-xs disabled" data-toggle="tooltip" data-placement="top" title="Reject Disabled"><i class="fa fa-trash-o"></i> Reject</button>');
							array_push($dd, '<span class="label label-success" style="padding: .6em 10px .3em;">Approved</span>');
						}else if($value->status==3){
							array_push($dd, '<span class="label label-danger" style="padding: .6em 10px .3em;">Rejected</span>');
							array_push($dd, '<span class="label label-success" style="padding: .6em 10px .3em;">Approved</span>');
						}else if($value->status==4){
							array_push($dd, '<button class="btn btn-default btn-xs disabled" data-toggle="tooltip" data-placement="top" title="Reject Disabled"><i class="fa fa-trash-o"></i> Reject</button>');
							array_push($dd, '<button class="btn btn-default btn-xs disabled" data-toggle="tooltip" data-placement="top" title="Approve Disabled"><i class="fa fa-check"></i> Approve</button>');
						}
					}
					
					array_push($jsonList, $dd);
					$i++;				

				}
				return Response::json(array('data' => $jsonList));
			} else {
				return Response::json(array('data' => []));
			}
		} else {
			return Response::json(array('data' => []));
		}
	}

    /**
     * Reject Request
     */
    public function rejectRequest(RequestManage $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');

            $data = RequestDetail::find($id);
            if ($data) {
                $data->status = 3;
                $data->save();

                $res=RequestApprove::where('request_id',$id)->whereNull('approved_date')->first();

                if($res){
                	$res->status_id=3;
                	$res->approved_date=Carbon::now();
                	$res->save();
                }

                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Approve Request
     */
    public function approveRequest(RequestManage $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');

            $data = RequestDetail::find($id);
            if ($data) {

            	$res=RequestApprove::where('request_id',$id)->whereNull('approved_date')->first();
            	
            	if($data->request_type_id==1){
	                
	                $data->status = 1;
	                $data->save();

	                if($res){
	                	$res->status_id=2;
	                	$res->approved_date=Carbon::now();
	                	$res->save();
	                }

	                $employee= Employee::find(Sentinel::getUser()->employee_id);

					if($employee->parent && $employee->parent!=1){
						
						$result=RequestApprove::create([
							'request_id'=>$res->id,
							'approved_employee_id'=>$employee->parent,
							'status_id'=>1
						]);

					}else{
						$data->status = 2;
	                	$data->save();	
					}					

	                return response()->json(['status' => 'success']);

            	}else{
            		$data->status = 2;
	                $data->save();

	                if($res){
	                	$res->status_id=2;
	                	$res->approved_date=Carbon::now();
	                	$res->save();
	                }            		
                	return response()->json(['status' => 'success']);
            	}

            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Approve Request
     */
    public function setReplacementDetail(RequestManage $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');
            $submit_qty = $request->input('submit_qty');

            $data = RequestDetail::find($id);
            if ($data) {
                
                $res=RequestApprove::where('request_id',$id)->whereNull('approved_date')->first();

                $data->status = 2;
                $data->approved_quantity = $submit_qty;
                $data->save();

                if($res){
                	$res->status_id=2;
                	$res->approved_date=Carbon::now();
                	$res->save();
                }					

                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * To Service Request
     */
    public function sentToService(RequestManage $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');

            $data = RequestDetail::find($id);
            if ($data) {
                $data->status = 4;
                $data->save();                
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

}
