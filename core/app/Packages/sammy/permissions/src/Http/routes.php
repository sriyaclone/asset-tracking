<?php
/**
 * PERMISSIONS MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Yasith Samarawickrama <yazith11@gmail.com>
 * @copyright 2015 Yasith Samarawickrama
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'permission', 'namespace' => 'Sammy\Permissions\Http\Controllers'], function(){
      
      /**
       * GET Routes
       */

      Route::get('list', [
        'as' => 'permission.list', 'uses' => 'PermissionController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'permission.list', 'uses' => 'PermissionController@jsonList'
      ]);

      Route::get('api/list', [
        'as' => 'permission.api.list', 'uses' => 'PermissionController@apiList'
      ]);

      /**
       * POST Routes
       */

      Route::post('status', [
        'as' => 'permission.status', 'uses' => 'PermissionController@status'
      ]);

      Route::post('delete', [
        'as' => 'permission.delete', 'uses' => 'PermissionController@delete'
      ]);
    });
});