<style type="text/css">

    td{
        font-weight: 400;
        font-size: 8px;
    }

    .details , .details> th ,  .details> td {
        border-collapse: collapse;
        padding: 5px;
    }

    th {
        text-align: left;
    }


    .border{
        border-width:1px;
        border-style:solid;
    }

    .border-left{
        border-left-style:solid;
        border-width:1px;
    }

    .border-right{
        border-right-style:solid;
        border-width:1px;
    }

    .border-top{
        border-top-style:solid;
        border-width:1px;
    }

    .border-bottom {
        border-bottom-style:solid;
        border-width:1px;
    }

</style>

<table width="100%" style="padding-top: 45px">
    <tbody>
        <?php $i=1; ?>
        <tr>
            @foreach($assets as $asset)
                <td>
                    <table style="width:100%" border="1">
                        <tr>
                            <td>
                                <i style="font-size: 9px;width: 100%" class="text-center">Inventory : {{$asset->inventory_no}}</i>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <img style="width: 25px;padding-top: 20px" src="{{asset('assets/sammy_new/images/rdb.jpg')}}">
                                        </td>
                                        <td width="79%">
                                            <span style="width:100%">
                                                <?php echo '<img style="" src="data:image/png;base64,' . DNS1D::getBarcodePNG($asset->inventory_no, $asset->barcode->barcodetype->name,1,40) . '" alt="barcode"   />' ?> <br>
                                            </span>
                                        </td>
                                        <td width="1%"></td>
                                    </tr>
                                </table>        
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i style="font-size: 9px;width: 100%;" class="text-center">Serial : {{$asset->asset_no}}</i>
                            </td>
                        </tr>
                    </table>
                </td>
                @if($i==3)
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%" border="1">
                                <tr>
                                    <td>
                                        <i style="font-size: 9px;width: 100%" class="text-center">Inventory : {{$asset->inventory_no}}</i>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <img style="width: 25px;padding-top: 20px" src="{{asset('assets/sammy_new/images/rdb.jpg')}}">
                                                </td>
                                                <td width="79%">
                                                    <span style="width:100%">
                                                        <?php echo '<img style="" src="data:image/png;base64,' . DNS1D::getBarcodePNG($asset->inventory_no, $asset->barcode->barcodetype->name,1,40) . '" alt="barcode"   />' ?> <br>
                                                    </span>
                                                </td>
                                                <td width="1%"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i style="font-size: 9px;width: 100%;" class="text-center">Serial : {{$asset->asset_no}}</i>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    <?php $i=1; ?>
                @endif        
                <?php $i++; ?>
            @endforeach
        </tr>
    </tbody>
</table>
