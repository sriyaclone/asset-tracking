<?php

namespace Sammy\PermissionGroups\Models;

use Illuminate\Database\Eloquent\Model;

class RoleGroup extends Model{
    protected $table = 'role_groups';
}
