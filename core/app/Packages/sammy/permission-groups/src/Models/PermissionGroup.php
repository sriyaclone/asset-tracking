<?php

namespace Sammy\PermissionGroups\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model{
    protected $table = 'permission_groups';
}
