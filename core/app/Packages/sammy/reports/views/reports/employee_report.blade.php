@extends('layouts.sammy_new.master') @section('title','Employee List')
@section('css')
<style type="text/css">

</style>
@stop
@section('content')
<ol class="breadcrumb">
	<li>
    	<a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a>
  	</li>
  	<li class="active">Employee List</li>
</ol>

<div class="row">
	<div class="col-xs-12">
		<form role="form" class="form-validation" method="get">
			<div class="panel panel-bordered" id="panel-search">
				<div class="panel-heading border">
					<strong>Employee Summary Report</strong>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="form-group col-md-3">
							<label class="control-label">Search</label>
							<input type="text" name="keyword" id="keyword" class="form-control" value="{{$old['keyword']}}">
						</div>

						<div class="form-group col-md-2">
							<label class="control-label">Employee Code</label>
							<input type="text" name="code" id="code" class="form-control" value="{{$old['code']}}">
						</div>

						<div class="form-group col-md-3">
							<label class="control-label">Name</label>
							<input type="text" name="name" id="name" class="form-control" value="{{$old['name']}}">
						</div>

						<div class="form-group col-md-2">
							<label class="control-label">Role</label>
							<select name="type" id="type" class="form-control chosen" required="required">
								<option value="0">-ALL-</option>
								@foreach($types as $type)
									<option value="{{$type->id}}" @if($type->id==$old['type']) selected @endif>{{$type->name}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-md-2">
							<label class="control-label">Status</label>
							<select name="status" id="status" class="form-control chosen" required="required">
								@foreach($status as $key=>$status)
									<option value="{{$key}}" @if($key==$old['status']) selected @endif>{{$status}}</option>
								@endforeach
							</select>
						</div>

					</div>
					
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="pull-left" style="margin-top:8px">
								<select name="location" id="location" class="form-control chosen" required="required">
									@foreach($locations as $key=>$location)
										<option value="{{$key}}" @if($key==$old['location']) selected @endif>{{$location}}</option>
									@endforeach
								</select>
							</div>

							<div class="pull-left" style="margin-top:15px;margin-left: 10px">
								<div class="form-group">
									<input type="checkbox" name="sub_loc" id="sub_loc" value="1" @if($old['sub_loc'] == 1) checked @endif>
			      					<label class="control-label">With Sub Location</label>
			            		</div>
							</div>

							<div class="pull-right" style="margin-top:8px;margin-left:8px">
								<button type="button" class="btn btn-danger btn-pdf">
									<i class="fa fa-download"></i> PDF
								</button>
							</div>
							
							<div class="pull-right" style="margin-top:8px;margin-left:8px">
								<button type="button" class="btn btn-warning btn-excel">
									<i class="fa fa-download"></i> Excel
								</button>
							</div>
							<div class="pull-right" style="margin-top:8px">
								<button type="submit" class="btn btn-primary btn-search">
									<i class="fa fa-check"></i> search
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>	
	</div>
</div>


<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-bordered">
      		<div class="panel-body" style="overflow-x:scroll">
      			<table class="table table-bordered">
          			<thead style="background:#ddd">
	                  	<th class="text-center" width="2%">#</th>
	                  	<th class="text-center">Type</th>
	                  	<th class="text-center">Code</th>
	                  	<th class="text-center">Name</th>
	                  	<th class="text-center">NIC</th>
	                  	<th class="text-center">Address</th>
	                  	<th class="text-center">Mobile</th>
	                  	<th class="text-center">Email</th>
	                  	<th class="text-center">Location</th>
	                  	<th class="text-center">Superior</th>
	                  	<th class="text-center">Status</th>
	                  	<th class="text-center">Created By</th>
	                  	<th class="text-center" width="15%">Created Date</th>
	                  	<th class="text-center">Verified User</th>
	                  	<th class="text-center" width="15%">Verified Date</th>
          			</thead>
          			<tbody>
	              		@foreach($data as $key=>$employee)
	              			<tr>
		              			<td>{{$key+1}}</td>
		              			<td>{{$employee->type->name!=null?$employee->type->name:'-'}}</td>
		              			<td>{{$employee->code}}</td>
		              			<td>
		              				{{$employee->first_name}} {{$employee->last_name}} <br>
		              				@if($employee->branch_user==1)
										<span class="badge" style="font-size: 10px;background-color: navy">Branch User</span>
									@endif
		              			</td>
		              			<td>{{$employee->nic}}</td>
		              			<td>{{$employee->address}}</td>
		              			<td>{{$employee->mobile!=null?$employee->mobile:'-'}}</td>
		              			<td>{{$employee->email!=null?$employee->email:'-'}}</td>
		              			<td>
		              				@if($employee->location!=null)
		              					{{$employee->location->location->name}}
		              				@else
						               -
						            @endif
		              			</td>
		              			<td>{{($employee->parentEmployee)?$employee->parentEmployee->full_name:'-'}}</td>
		              			<td>
		              				@if($employee->status==1)                
						                <span class="label label-success">Activated</span>
						            @elseif($employee->status==2)
						                <span class="label label-warning">Rejected</span>
						            @elseif($employee->status==0)
						                <span class="label label-danger">Deactivated</span>
						            @endif
		              			</td>

		              			<td class="text-center">
									@if($employee->createdBy)
										{{$employee->createdBy->first_name}} {{$employee->createdBy->last_name}}
									@else
										-
									@endif
								</td>

								<td class="text-center">
									@if($employee->created_at)
										{{$employee->created_at}}
									@else
										-
									@endif
								</td>

								<td class="text-center">
									@if($employee->verifiedby)
										{{$employee->verifiedby->first_name}} {{$employee->verifiedby->last_name}}
									@else
										-
									@endif
								</td>

								<td class="text-center">
									@if($employee->varified_date)
										{{$employee->varified_date}}
									@else
										-
									@endif
								</td>

				            </tr>
	              		@endforeach
	              	</tbody>
          		</table>
          		<?php echo $data->appends(Input::except('page'))->render()?>
          		<br>
          		<?php echo "Total Row Count : ".$row_count?>
      		</div>
      	</div>	
	</div>
</div>

@stop
@section('js')
<script src="{{asset('assets/sammy_new/vendor/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
	var id = 0;

	$(document).ready(function(){
		$('.btn-excel').click(function(e){
			e.preventDefault();

        	loc = $("#location").val();
        	sub_loc = $("#sub_loc").val();
        	code = $("#code").val();
        	status = $("#status").val();
        	name = $("#name").val();
        	keyword = $("#keyword").val();
        	type = $("#type").val();


        	window.location.href = '{{url('reports/employee-summery-report/download/excel')}}?location='+loc+'&sub_loc='+sub_loc+'&code='+code+'&status='+status+'&name='+name+'&keyword='+keyword+'&type='+type;

		});

		$('.btn-pdf').click(function(e){
			e.preventDefault();

        	loc = $("#location").val();
        	sub_loc = $("#sub_loc").val();
        	code = $("#code").val();
        	status = $("#status").val();
        	name = $("#name").val();
        	keyword = $("#keyword").val();
        	type = $("#type").val();

        	window.open('{{url('reports/employee-summery-report/download/pdf')}}?location='+loc+'&sub_loc='+sub_loc+'&code='+code+'&status='+status+'&name='+name+'&keyword='+keyword+'&type='+type,'_blank');

		});
	});

	function pageReload(){
		window.location.href = window.location.href;
	}
</script>
@stop