@extends('layouts.sammy_new.master') @section('title','User List')
@section('css')
<style type="text/css">

</style>
@stop
@section('content')
<ol class="breadcrumb">
	<li>
    	<a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a>
  	</li>
  	<li>
    	<a href="#">Report Management</a>
  	</li>
  	<li class="active">User Summery List</li>
</ol>

<div class="row">
	<div class="col-xs-12">
		<form role="form" class="form-validation" method="get">
			<div class="panel panel-bordered" id="panel-search">
				<div class="panel-heading border">
					<strong>User Summary Report</strong>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="form-group col-md-4">
							<label class="control-label">Search Keyword</label>
							<input type="text" name="search_keyword" id="search_keyword" class="form-control" value="{{$old['search_keyword']}}" placeholder="Enter Username, First Name, Last Name, Code">
						</div>
						<div class="form-group col-md-4">
							<label class="control-label">Type</label>
							<select name="type" id="type" class="form-control chosen" required="required">
								<option value="0">-ALL-</option>
								@foreach($types as $type)
									<option value="{{$type->id}}" @if($type->id==$old['type']) selected @endif>{{$type->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-4">
							<label class="control-label">Status</label>
							<select name="status" id="status" class="form-control chosen" required="required">
								@foreach($status as $key=>$status)
									<option value="{{$key}}" @if($key==$old['status']) selected @endif>{{$status}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-3">
							<div class="form-group" style="margin-top:8px">
								<label class="control-label">Location</label>
								<select name="location" id="location" class="form-control chosen" required="required">
									@foreach($locations as $key=>$location)
										<option value="{{$key}}" @if($key==$old['location']) selected @endif>{{$location}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group col-md-6">
							<div class="pull-left" style="margin-top:15px;margin-left: 10px">
								<div class="form-group" style="margin-bottom: 0; margin-top: 15px;">
									<input type="checkbox" name="sub_loc" id="sub_loc" value="1" @if($old['sub_loc'] == 1) checked @endif>
									<label class="control-label">With Sub Location</label>
								</div>
							</div>
						</div>
						<div class="form-group col-md-3">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="pull-right" style="margin-top:8px;margin-left:8px">
										<button type="button" class="btn btn-danger btn-pdf">
											<i class="fa fa-download"></i> PDF
										</button>
									</div>
									
									<div class="pull-right" style="margin-top:8px;margin-left:8px">
										<button type="button" class="btn btn-warning btn-excel">
											<i class="fa fa-download"></i> Excel
										</button>
									</div>
									<div class="pull-right" style="margin-top:8px">
										<button type="submit" class="btn btn-primary btn-search">
											<i class="fa fa-check"></i> search
										</button>
									</div>

								</div>
							</div>	
						</div>
					</div>

				</div>
			</div>
		</form>	
	</div>
</div>


<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-bordered">
      		<div class="panel-body" style="overflow-x:scroll">
      			<table class="table table-bordered">
          			<thead style="background:#ddd">
	                  	<th class="text-center" width="2%">#</th>
	                  	<th class="text-center">User Name</th>
	                  	<th class="text-center" width="15%">First Name</th>
	                  	<th class="text-center">Last Name</th>
	                  	<th class="text-center">User Role</th>
	                  	<th class="text-center">Location</th>
	                  	<th class="text-center">Email</th>
	                  	<th class="text-center" width="15%">Last Login</th>
	                  	<th class="text-center">Created By</th>
	                  	<th class="text-center" width="15%">Created Date</th>
	                  	<th class="text-center">Verified User</th>
	                  	<th class="text-center" width="15%">Verified Date</th>
	                  	<th class="text-center">Varification Status</th>
	                  	<th class="text-center">Status</th>
          			</thead>
          			<tbody>
          				<?php $i=1; ?>
          				@foreach($data as $userDetail)
          					<tr>
          						<td class="text-center">{{$i}}</td>
								<td class="text-center">@if($userDetail->username != ""){{$userDetail->username}} @else - @endif</td>
								<td class="text-center">@if($userDetail->employee[0]->first_name != ""){{$userDetail->employee[0]->first_name}}@else - @endif</td>
								<td class="text-center">@if($userDetail->employee[0]->last_name != ""){{$userDetail->employee[0]->last_name}}@else - @endif</td>
								<td class="text-center">@if(count($userDetail->UserRole)>0){{$userDetail->UserRole[0]->name}}@else - @endif</td>
								<td class="text-center">@if($userDetail->employee[0]->Location){{$userDetail->employee[0]->location->location->name}}@else - @endif</td>
								<td class="text-center">@if($userDetail->employee[0]->email != ""){{$userDetail->employee[0]->email}}@else - @endif</td>
								<td class="text-center">@if($userDetail->last_login != ""){{$userDetail->last_login}}@else - @endif</td>

								<td class="text-center">
									@if($userDetail['createdBy'])
										{{$userDetail['createdBy']['employee'][0]->first_name}} {{$userDetail['createdBy']['employee'][0]->last_name}}
									@else
										-
									@endif
								</td>

								<td class="text-center">
									@if($userDetail->created_at)
										{{$userDetail->created_at}}
									@else
										-
									@endif
								</td>

								<td class="text-center">
									@if($userDetail->verifiedby)
										{{$userDetail->verifiedby->first_name}} {{$userDetail->verifiedby->last_name}}
									@else
										-
									@endif
								</td>

								<td class="text-center">
									@if($userDetail->varified_date)
										{{$userDetail->varified_date}}
									@else
										-
									@endif
								</td>


								@if($userDetail->varified_status == 0)
									
									<td class="text-center">
										<label class="label label-danger"><span>Not Verified</span></label>
									</td>
								@elseif($userDetail->varified_status == 1)
									<td class="text-center">
										<label class="label label-success"><span>Verified</span></label>
									</td>
								@elseif($userDetail->varified_status == 2)
									<td class="text-center">
										<label class="label label-danger"><span>Rejected</span></label>
									</td>
								@endif

								@if($userDetail->status == 1)
									<td class="text-center">
										<label class="label label-success"><span>Active</span></label>
									</td>
								@else
									<td class="text-center">
										<label class="label label-danger"><span>Deactive</span></label>
									</td>
								@endif									

          					</tr>
          				<?php $i++; ?>
          				@endforeach
          			</tbody>
          		</table>
          		<?php echo $data->appends(Input::except('page'))->render()?>
                <br>
                <?php echo "Total Row Count : ".$row_count?>
      		</div>
      	</div>	
	</div>
</div>

@stop
@section('js')
<script src="{{asset('assets/sammy_new/vendor/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
	var id = 0;

	$(document).ready(function(){
		$('.btn-excel').click(function(e){
			e.preventDefault();

        	loc = $("#location").val();
        	search_keyword = $("#search_keyword").val();
        	status = $("#status").val();
        	type = $("#type").val();
        	sub_loc = $("#sub_loc").val();


        	window.location.href = '{{url('reports/user-summery-report/download/excel')}}?location='+loc+'&search_keyword='+search_keyword+'&status='+status+'&type='+type+'&sub_loc='+sub_loc;

		});

		$('.btn-pdf').click(function(e){
			e.preventDefault();

        	loc = $("#location").val();
        	search_keyword = $("#search_keyword").val();
        	status = $("#status").val();
        	type = $("#type").val();
			sub_loc = $("#sub_loc").val();

        	window.open('{{url('reports/user-summery-report/download/pdf')}}?location='+loc+'&search_keyword='+search_keyword+'&status='+status+'&type='+type+'&sub_loc='+sub_loc,'_blank');

		});
	});

	function pageReload(){
		window.location.href = window.location.href;
	}
</script>
@stop