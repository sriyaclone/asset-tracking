@extends('layouts.sammy_new.master') @section('title','Software List')
@section('css')
<style type="text/css">

</style>
@stop
@section('content')
<ol class="breadcrumb">
	<li>
    	<a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a>
  	</li>
  	<li>
    	<a href="#">Report Management</a>
  	</li>
  	<li class="active">Software Summery List</li>
</ol>

<div class="row">
	<div class="col-xs-12">
		<form role="form" class="form-validation" method="get">
			<div class="panel panel-bordered" id="panel-search">			
				<div class="panel-body">
					<div class="row">
						<div class="form-group col-md-4">
							<label class="control-label">License</label>
							<select name="license" id="license" class="form-control chosen" required="required">
								<option value="0">---- Select License -----</option>
								@foreach($licenses as $license)
									<option value="{{$license->id}}" @if($license->id==$old['license_id']) selected @endif>{{$license->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-4">
							<label class="control-label">Location</label>
							<select name="location" id="location" class="form-control chosen" required="required">
								@foreach($locations as $key=>$location)
									<option value="{{$key}}" @if($key==$old['location']) selected @endif>{{$location}}</option>
								@endforeach
							</select>
							<label class="control-label">With Sub Location</label>
							<input type="checkbox" name="sub_loc" id="sub_loc" @if($old['sub_loc']) checked @endif style="margin-top: 5px">
						</div>

						<div class="form-group col-md-4">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="pull-right" style="margin-top:25px;margin-left:8px">
										<button type="button" class="btn btn-danger btn-pdf">
											<i class="fa fa-download"></i> PDF
										</button>
									</div>
									
									<div class="pull-right" style="margin-top:25px;margin-left:8px">
										<button type="button" class="btn btn-warning btn-excel">
											<i class="fa fa-download"></i> Excel
										</button>
									</div>
									<div class="pull-right" style="margin-top:25px">
										<button type="submit" class="btn btn-primary btn-search">
											<i class="fa fa-check"></i> search
										</button>
									</div>

								</div>
							</div>
						</div>
						
					</div>

				</div>
			</div>
		</form>	
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-bordered">
			<div class="panel-header" style="text-align: center">
				<h4 style="padding: 10px">Software Asset Usage Report</h4>
			</div>

			<div class="panel-body">
				<div class="row">
					@if($data_license)
					<div class="col-sm-6">
						
						<table class="table table-bordered" style="width: 60%">
							<tr>
								<td style="font-weight: bolder">No of License</td>
								<td style="font-weight: bolder">{{$data_license->max_users}}</td>
							</tr>
							<tr>
								<td style="font-weight: bolder">Usage of License</td>
								<td style="font-weight: bolder">{{$used_count}}</td>
							</tr>
							<tr>
								<td style="font-weight: bolder">Remaining License</td>
								<td style="font-weight: bolder">{{$data_license->max_users-$used_count}}</td>
							</tr>
						</table>						
					</div>
					<div class="col-sm-6">
						<table class="table table-bordered pull-right" style="width: 60%">
							<tr>
								<td style="font-weight: bolder"><span>Supplier</span></td>
								<td style="font-weight: bolder"><span>{{$data_license['supplier']->name}}</span></td>
							</tr>
							<tr>
								<td style="font-weight: bolder"><span>License Start Date</span></td>
								<td style="font-weight: bolder"><span>{{$data_license->license_from}}</span></td>
							</tr>
							<tr>
								<td style="font-weight: bolder"><span>License End Date</span></td>
								<td style="font-weight: bolder"><span>{{$data_license->license_to}}</span></td>
							</tr>
						</table>
					</div>
					@endif
				</div>
			</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-bordered">
      		<div class="panel-body" style="overflow-x:scroll">
      			<table class="table table-bordered">
          			<thead style="background:#ddd">
	                  	<th class="text-center" width="2%">#</th>
	                  	<th class="text-center">Inventory No</th>
	                  	<th class="text-center" width="15%">Serial No</th>
	                  	<th class="text-center">Manufacture</th>
	                  	<th class="text-center">Model</th>
	                  	<th class="text-center">IP Address</th>
	                  	<th class="text-center">Reference No</th>
	                  	<th class="text-center">Install Date</th>
	                  	<th class="text-center">Expiry Date</th>
          			</thead>
          			<tbody>
          				<?php $i=1; ?>
          				@foreach($data as $softwareDetail)
          					<tr>
          						<td>{{$i}}</td>
          						<td>{{$softwareDetail['asset']->inventory_no}}</td>
          						<td>{{$softwareDetail['asset']->asset_no}}</td>
          						<td>{{$softwareDetail['asset']['manufacturer']->name}}</td>
          						<td>{{$softwareDetail['asset']->model}}</td>
          						<td>{{$softwareDetail->note}}</td>
          						<td>{{$softwareDetail->reference_no}}</td>
          						<td>{{date_format($softwareDetail->created_at, 'Y-M-d')}}</td>
          						<td>{{$softwareDetail['software']->license_from}}</td>
          					</tr>
          				<?php $i++; ?>
          				@endforeach
          			</tbody>
          		</table>
          		<?php echo $data->appends(Input::except('page'))->render()?>
                <br>
                <?php echo "Total Row Count : ".$row_count?>
      		</div>
      	</div>	
	</div>
</div>

@stop
@section('js')
<script src="{{asset('assets/sammy_new/vendor/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
	var id = 0;

	$(document).ready(function(){
		$('.btn-excel').click(function(e){
			e.preventDefault();

        	loc = $("#location").val();
        	license = $("#license").val();
        	sub_loc = $("#sub_loc").val();

        	window.location.href = '{{url('reports/software-summery-report/download/excel')}}?location='+loc+'&license='+license+'&sub_loc='+sub_loc;

		});

		$('.btn-pdf').click(function(e){
			e.preventDefault();

        	loc = $("#location").val();
        	license = $("#license").val();
        	sub_loc = $("#sub_loc").val();

        	window.open('{{url('reports/software-summery-report/download/pdf')}}?location='+loc+'&license='+license+'&sub_loc='+sub_loc,'_blank');

		});
	});

	function pageReload(){
		window.location.href = window.location.href;
	}
</script>
@stop