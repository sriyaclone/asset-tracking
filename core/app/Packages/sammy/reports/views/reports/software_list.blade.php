@extends('layouts.sammy_new.master') @section('title','Software List')
@section('css')
<style type="text/css">

</style>
@stop
@section('content')
<ol class="breadcrumb">
	<li>
    	<a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a>
  	</li>
  	<li>
    	<a href="#">Report Management</a>
  	</li>
  	<li class="active">Software Summery List</li>
</ol>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-bordered">
			<div class="panel-header" style="text-align: center">
				<form method="get">
					<h4 style="padding: 10px">Software Asset Report</h4>
					<div class="container-fluid">
						<div class="row">
							<div class="form-group col-md-3">
								<label class="control-label">Serial No.</label>
								<input type="text" class="form-control" name="serial" value="{{$old->serial}}">
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Software Name</label>
								<input type="text" class="form-control" name="name" value="{{$old->name}}">
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Supplier</label>
								<select name="supplier" id="location" class="form-control chosen">
									<option value="">-- ALL --</option>
									@foreach($suppliers as $key => $supplier)
										<option value="{{$key}}" @if($key==$old->supplier) selected @endif>{{$supplier}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Manufacturer</label>
								<select name="manufacturer" id="manufacturer" class="form-control chosen">
									<option value="">-- ALL --</option>
									@foreach($manufacturers as $key => $manufacturer)
										<option value="{{$key}}" @if($key==$old->manufacturer) selected @endif>{{$manufacturer}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="pull-right" style="margin-left:8px;margin-right: 15px">
									<button type="button" class="btn btn-danger btn-pdf">
										<i class="fa fa-download"></i> PDF
									</button>
								</div>

								<div class="pull-right" style="margin-left:8px">
									<button type="button" class="btn btn-warning btn-excel">
										<i class="fa fa-download"></i> Excel
									</button>
								</div>
								<div class="pull-right" style="margin-left:8px">
									<button type="submit" class="btn btn-primary btn-search">
										<i class="fa fa-check"></i> Search
									</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="panel panel-bordered">
				      		<div class="panel-body" style="overflow-x:scroll">
				      			<table class="table table-bordered">
				          			<thead style="background:#ddd">
					                  	<th class="text-center" width="2%">#</th>
					                  	<th class="text-left" width="15%">Serial No</th>
					                  	<th class="text-center">Software Name</th>
					                  	<th class="text-center">Supplier Name</th>
					                  	<th class="text-center">Manufacturer</th>
					                  	<th class="text-center">No of License</th>
					                  	<th class="text-center">No of Allocation</th>
					                  	<th class="text-center">Remaining</th>
					                  	<th class="text-center">License Start Date</th>
					                  	<th class="text-center">License Expiry Date</th>
				          			</thead>
				          			<tbody>
				          				<?php $i=1; ?>
				          				@foreach($data as $software)
				          					<tr>
				          						<td>{{$i}}</td>
				          						<td class="text-left">{{$software->asset_no}}</td>
				          						<td class="text-center">{{$software->name}}</td>
				          						<td class="text-center">{{$software['supplier']->name}}</td>
				          						<td class="text-center">{{$software['manufacture']->name}}</td>
				          						<td class="text-center">{{$software->max_users}}</td>
				          						<td class="text-center">{{count($software['details'])}}</td>
				          						<td class="text-center">{{$software->max_users - count($software['details'])}}</td>
				          						<td class="text-center">{{$software->license_from}}</td>
				          						<td class="text-center">{{$software->license_to}}</td>
				          					</tr>
				          				<?php $i++; ?>
				          				@endforeach
				          			</tbody>
				          		</table>
				          		<?php echo $data->appends(Input::except('page'))->render()?>
				                <br>
				                <?php echo "Total Row Count : ".count($data)?>
				      		</div>
				      	</div>	
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

@stop
@section('js')
<script src="{{asset('assets/sammy_new/vendor/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
	var id = 0;

	$(document).ready(function(){
		$('.btn-excel').click(function(e){
			e.preventDefault();

			var query = window.location.search;

        	window.location.href = '{{url('reports/software-report/download/excel')}}'+query;

		});

		$('.btn-pdf').click(function(e){
			e.preventDefault();

			var query = window.location.search;

        	window.open('{{url('reports/software-report/download/pdf')}}'+query,'_blank');

		});
	});

	function pageReload(){
		window.location.href = window.location.href;
	}
</script>
@stop