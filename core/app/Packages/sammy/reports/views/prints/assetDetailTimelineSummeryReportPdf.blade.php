<style type="text/css">
    
</style>

<div class="row">
    <div class="col-xs-12 col-md-12 pull-left">
        <div class="panel panel-bordered">
            <div class="panel-body">
                <div class="row" id="print-details">
                    <table class="table table-bordered" border="1">
                        <thead>
                            <tr>
                                <th>Action Date</th>
                                <th>Action Title</th>
                                <th>Location</th>
                                <th>Employee Location</th>
                                <th>Action By</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php $lk=0;?>
                            @foreach($timeline as $data)
                                <tr>
                                    <td>{{date_format($data->created_at, 'D-M-d-Y')}}<br>{{date_format($data->created_at, 'h:i:s')}}</td>
                                    @if($lk==count($timeline)-1)
                                        <td>{{$data->type}}</td>
                                        <td>{{$data->location->name}}</td>
                                        <td></td>
                                        <td>{{$data->user->first_name}} {{$data->user->last_name}} <br> @if(isset($data->user->code)){{$data->user->code}}@endif</td>
                                    @elseif($lk==count($timeline)-2)
                                        <td>{{$data->type}}</td>
                                        <td>{{$data->location->name}}</td>
                                        <td></td>
                                        <td>{{$data->user->first_name}} {{$data->user->last_name}} <br> @if(isset($data->user->code)){{$data->user->code}}@endif</td>
                                    @else
                                        <td>{{$data->type}}</td>
                                        @if($data->status->id==ASSET_UNDEPLOY)
                                            <td>{{$data->location->name}}</td>                         
                                            <td></td>                         
                                        @elseif($data->status->id==ASSET_DEPLOY)                            
                                            <td>{{$data->location->name}}</td>
                                            <td>@if($data->employee){{$data->employee->first_name}} {{$data->employee->last_name}}@endif</td>
                                        @else
                                            <td>{{$data->location->name}}</td>
                                            <td></td>
                                        @endif
                                        <td>{{$data->user->first_name}} {{$data->user->last_name}} <br> @if(isset($data->user->code)){{$data->user->code}}@endif</td>
                                    @endif
                                </tr>
                                <?php $lk++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>  
</div>